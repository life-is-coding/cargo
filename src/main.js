import Vue from "vue"
import App from "./App.vue"
import vuetify from "./plugins/vuetify"
import store from "./store"
import router from "./router"
import VueFbCustomerChat from "vue-fb-customer-chat"
import VueSweetalert2 from "vue-sweetalert2"
import "sweetalert2/dist/sweetalert2.min.css"
import * as VueAos from "vue-aos"
import AOS from "aos"
import "aos/dist/aos.css"
import VueLazyload from "vue-lazyload"

Vue.use(VueLazyload)
Vue.use(VueAos)
require("./assets/main.css")
Vue.use(VueSweetalert2)
Vue.use(VueFbCustomerChat, {
    page_id: 143816855814744, //  change 'null' to your Facebook Page ID,
    theme_color: "#333333", // theme color in HEX
    locale: "en_US", // default 'en_US'
})

Vue.config.productionTip = false

new Vue({
    created() {
        AOS.init()
    },
    vuetify,
    render: (h) => h(App),
    store,
    router,
}).$mount("#app")