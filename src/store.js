import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        miniSidebar: true,
        drawer: null,
        isLoading: false,
        snackbar: false,
        chk_token: false,
    },
    mutations: {
        mutateMiniSidebar(state, obj) {
            if (obj.isMobile) {
                // mobile
                state.miniSidebar = false
                state.drawer = null
                if (!obj.navShow) {
                    state.drawer = null
                } else {
                    state.drawer = !state.drawer
                }
            } else {
                // others
                state.miniSidebar = obj.isMobile ?
                    state.miniSidebar :
                    !state.miniSidebar
                state.drawer = null
            }
        },
        mutateIsLoading(state) {
            state.isLoading = !state.isLoading
        },
        mutatesnackbar(state) {
            state.snackbar = !state.snackbar
                // state.timeout = state.timeout
        },
        mutateToken(state) {
            state.chk_token = !state.chk_token
        },
    },
})

export default store