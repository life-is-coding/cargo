import Vue from "vue"
import VueRouter from "vue-router"

// import xx from "./layouts/pages/components/SearchLink.vue"
//
Vue.use(VueRouter)

const routes = [{
    path: "/",
    // component: xx,
    // meta: {
    //     title: "q test",
    // },
}, ]

const router = new VueRouter({
    routes: routes,
    mode: "history",
})

router.beforeEach((to, from, next) => {
    // console.log(to.path)
    // console.log(from.path)
    next()
})

export default router